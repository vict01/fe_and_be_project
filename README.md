# Test to cover UI and API part
## This is the test automation framework based on Javascript, NodeJS, Jasmine, Selenium-Webdriver and Artillery
## Used to create tests easily for Web Browser Navitagation and API calls.

This framework is made up of the following tools:
```
1. Javascript: The programing language to build the test logic.
2. NodeJS: The Runtime environment that allows packaging and manage the dependencies.
3. Jasmine: The testing framework that provides a BDD and easy-to-read syntax 
4. Selenium webdriver: Framework used to automate the actions in the browser and locate elements.
5. Artillery: API cliente used to handling http requests.
```

## Test coverage
To find test coverage specifications:\
`UI_Testing\tests` folder, contains 2 files which describe scenarios for the UI part.\
`API_Testing\MarvelAPI.yaml` file, describes scenarios for the API/integration part.

## Framework Structure
`API_Testing` folder, contains the test focused on the API part\
`UI_Testing` folder, contains the test focused on the Web Browser parte

## Requirements Installation:
1. Unzip the file "E-Commerce-ProjectJS.rar" and place it on disk C.
2. Install Node JS.
3. Run `npm i` to install dependencies

## Instructions to run the tests:
1. Open terminal console and make sure you are in the root paht of the project, then run the tests by executing:\
     `npm run test` to execute the automation for UI (Web Browser)\
     `npm run APItesting` to execute the automation for API (Integration test)\
     `npm run APIreportGen` to generate the HTML report corresponding to the API test\
2. Pay attention while test runs in the browser or console, as applicable. 
3. Once test finish to run, in the console terminal of your IDE, you can see the testing summary report, with its corresponding steps, validations and results.\
   In the cases of the API testing, you also have a HTML report in the corresponding folder.
